﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ShowPoints : MonoBehaviour
{
    private PlayerController playerController;
    public Text texto;
    // Start is called before the first frame update
    void Start()
    {
        playerController = GetPlayer();
    }
    static PlayerController GetPlayer()
    {
        GameObject player;
        player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
        {
            return player.GetComponent<PlayerController>();
        }
        else
        {
            return null;
        }
    }
    // Update is called once per frame
    public void Update()
    {
       texto.text = Math.Round(playerController.Distance).ToString() ;
    }
}
