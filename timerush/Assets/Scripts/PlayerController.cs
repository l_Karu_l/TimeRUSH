﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class PlayerController : MonoBehaviour
{
    private Rigidbody thisRigidbody;
    private CharacterController controller;
    private Vector3 direction;
    private Transform transform;
    private CameraController cameracontroller;
    public GameObject textwin;

    public GameObject evan;
    public Transform CheckPoint1;
    public Transform Camara;
    public float forwardSpeed;
    public float jumpForce = 3f;
    public float Gravity = 9.8f;
    public float Distance = 0f;
    private float crouchHeight;
    private float standarHeight;
    public bool moriste = false;
    public bool ganaste = true;


    public int maxSaltos = 2;
    public int saltoActual;

    private float directionY;
    private bool canDoubleJump = false;
    [SerializeField] //cap de altura del doble salto
    private float doubleJumpCap = 0.5f;


    void Start()
    {
        controller = GetComponent<CharacterController>();
        thisRigidbody = GetComponent<Rigidbody>();
        standarHeight = controller.height;
        crouchHeight = standarHeight / 2.5f;
        transform = GetComponent<Transform>();
        cameracontroller = GetCamera();
    }

    void Crouching()
    {
        if (controller.isGrounded)
        {
            controller.height = crouchHeight;
            controller.center = new Vector3(0f, -0.5f, 0f);
        }
    }

    void GetUp()
    {
        //transform.position = new Vector3(transform.position.x, transform.position.y + 0.3f, transform.position.z);
        controller.center = new Vector3(0f, 0.22f, 0f);
        controller.height = standarHeight;

    }

    // Update is called once per frame
    void Update()
    {
        if (!moriste && !ganaste)
        {
            
            evan.GetComponent<Animation>().Play("run");
            direction = new Vector3(forwardSpeed, 0, 0);


            if (controller.isGrounded)
            {

                canDoubleJump = true;
                if (Input.GetKeyDown(KeyCode.UpArrow))
                {

                    directionY = jumpForce;
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.UpArrow) && canDoubleJump)
                {
                    directionY = jumpForce * doubleJumpCap;
                    canDoubleJump = false;
                }
            }

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                Crouching();
            }
            if (Input.GetKeyUp(KeyCode.DownArrow))
            {
                GetUp();
            }
            directionY -= Gravity * Time.deltaTime;
            direction.y = directionY;
            controller.Move(direction * Time.deltaTime);
        }
        else if(moriste)
        {
            StartCoroutine("Resucitar");
           
        }else if (ganaste)
        {
            textwin.SetActive(true);
        }
       
        Distance = Math.Abs(direction.x)+transform.position.x + 143f;
        Debug.Log("Distancia recorrida =" + Distance);
    }
    void OnTriggerEnter(Collider collision)
    {

        if (collision.gameObject.CompareTag("Enemy"))
        {
            Debug.Log("Ha entrado en la colision -" + CheckPoint1.GetComponent<Transform>().position);
            Debug.Log("Ha entrado en la colision -" + transform.position);
            Automorision();
        }
        else if (collision.gameObject.CompareTag("Finish"))
        {
            ganaste = true;
        }
    }
    public void Automorision()
    {
        
        Distance = 0;
        moriste = true;
        cameracontroller.velocidad = 0;

    }
    IEnumerator Resucitar()
    {
        yield return new WaitForSeconds(2f);
        moriste = false;
        controller.enabled = false;
        transform.position = CheckPoint1.GetComponent<Transform>().position;
        controller.enabled = true;
        Camara.position = new Vector3(CheckPoint1.GetComponent<Transform>().position.x, 3.6f, -7.79f);
        cameracontroller.velocidad = 6;
    }
    static CameraController GetCamera()
    {
        GameObject player;
        player = GameObject.FindGameObjectWithTag("MainCamera");
        if (player != null)
        {
            return player.GetComponent<CameraController>();
        }
        else
        {
            return null;
        }
    }
}
